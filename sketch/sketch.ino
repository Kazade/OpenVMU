
uint8_t RAM[512];

#include "rom.cpp"

// Registers
uint8_t* ACC = RAM + 0x100;
uint8_t* PSW = RAM + 0x101;
uint8_t* B = RAM + 0x102;
uint8_t* C = RAM + 0x103;
uint8_t* TRL = RAM + 0x104;
uint8_t* TRH = RAM + 0x105;
uint8_t* SP = RAM + 0x106;
uint8_t* PCON = RAM + 0x107;
uint8_t* IE = RAM + 0x108;
uint8_t* IP = RAM + 0x109;
uint8_t* EXT = RAM + 0x10D;
uint8_t* OCR = RAM + 0x10E;
uint8_t* T0CON = RAM + 0x110;
uint8_t* T0PRR = RAM + 0x111;
uint8_t* T0L = RAM + 0x112;
uint8_t* T0LR = RAM + 0x113;
uint8_t* T0H = RAM + 0x114;
uint8_t* T0HR = RAM + 0x115;

uint16_t PC = 0; // Program counter

uint16_t bytes_to_16(const uint8_t* first_byte) {
  return (first_byte[0] << 8) | first_byte[1];
}

inline void read_d9b3(const uint8_t* bytes, uint16_t* d9, uint8_t* b3) {
  *d9 = uint16_t(bytes[1]) | (uint16_t(bytes[0] & 0x10) << 4); // Get the 4th bit from the first byte, and add to the 8 bits of the second
  *b3 = bytes[0] & 0x7; // Get the last 3 bits of the first byte
}

inline void read_d9i8(const uint8_t* bytes, uint16_t* d9, uint8_t* i8) {
  *d9 = uint16_t(bytes[1]) | (uint16_t(bytes[0] & 0x01) << 8); // Get the first bit from the first byte, and add to the 8 bits of the second
  *i8 = bytes[2];
}

inline void read_d9(const uint8_t* bytes, uint16_t* d9) {
  // Combine the last bit of the first byte with the entire byte of the second
  *d9 = uint16_t(bytes[1]) | (uint16_t(bytes[0] & 0x01) << 8);
}

inline void read_a12(const uint8_t* bytes, uint16_t* addr) {
  // Combine the 12 bits with the upper 4 bits of the current program counter to
  // return a complete 16 bit address within the same 4k bank as PC
  *addr = uint16_t(bytes[1]) | (uint16_t(bytes[0] & 0xF) << 8) | (PC & 0xF000);
}

inline void read_address_rj(const uint8_t* bytes, uint16_t* addr) {
  // Read the 'j' value from bytes, and then look in the RAM to return
  // the final address for the operand  

  // This needs some serious explaining! OK, so the bank we choose depends on bits 3 and 4 (zero-based)
  // in the PSW register. We bitwise AND to extract just those bits, then we would normally bitshift
  // to the right by 3 to make this a sensible number (e.g. 1, 2, 3 or 4) however! Once we'd done that
  // we'd need to multiply by 4 to get the right offset (bitshift left 2 places). So we can short cut
  // and only bit shift to the right by 1, then we add the 'j' value from the last 2 bits of the byte
  // passed in, and the final cell address is the sum of them both (so basically (bank_selector * 4) + j)
  // Simples.
  uint8_t cell = ((*PSW & 0x18) >> 1) + (bytes[0] & 0x3);
  *addr = RAM[cell];
}

inline void writemem(uint16_t address, uint8_t byte) {
  RAM[address] = byte;

  switch(address) {
    case 0x108: // TODO: Change interrupts
    default:
      break;
  }
}

inline void stack_push(uint8_t val) {
  RAM[++(*SP)] = val;
}

inline void stack_pop(uint8_t* val) {
  *val = RAM[(*SP)--];
}

inline void set_carry() {
  *PSW |= (1 << 8);
}

inline void clear_carry() {
  *PSW &= ~(1 << 8);
}

inline void noop() {
  ++PC;
}

inline void clr1() {
  uint16_t addr;
  uint8_t bits;
  read_d9b3(&ROM[PC], &addr, &bits);
  writemem(addr, RAM[addr] & ~(1 << bits));
  PC += 2;
}

inline void set1() {
  uint16_t addr;
  uint8_t bits;
  read_d9b3(&ROM[PC], &addr, &bits);
  writemem(addr, RAM[addr] & (1 << bits));
  PC += 2;
}

inline void ld() {
  uint16_t addr;
  read_d9(&ROM[PC], &addr);
  *ACC = RAM[addr];
  PC += 2;
}

inline void mov_i8d9() {
  uint16_t d9;
  uint8_t i8;
  read_d9i8(&ROM[PC], &d9, &i8);
  writemem(d9, i8);
  Serial.print("MOV ");
  Serial.print(i8, HEX);
  Serial.print(" ");
  Serial.println(d9, HEX);
  PC += 3;
}

inline void mov_rj() {
  uint16_t addr;
  read_address_rj(&ROM[PC], &addr);
  writemem(addr, ROM[PC + 1]);
  Serial.print("MOV ");
  Serial.print(ROM[PC + 1], HEX);
  Serial.print(" ");
  Serial.println(addr, HEX);
  PC += 2;
}

inline void xor_i8() {
  *ACC ^= ROM[PC + 1];
  PC += 2;
}

inline void and_i8() {
  *ACC &= ROM[PC + 1];
  PC += 2;
}

inline void xor_d9() {
    uint16_t addr;
    read_d9(&ROM[PC], &addr);
    *ACC ^= RAM[addr];
    PC += 2;
}

inline void st_d9() {
    uint16_t addr;
    read_d9(&ROM[PC], &addr);
    RAM[addr] = *ACC;
    PC += 2;  
}

inline void inc_d9() {
    uint16_t addr;
    read_d9(&ROM[PC], &addr);
    RAM[addr]++;
    PC += 2;
}

inline void call_a12() {
    Serial.print("CALL: ");
    uint16_t addr;
    read_a12(&ROM[PC], &addr);
    PC += 2;
    
    // Push the address of the instruction following this one lowest byte first
    stack_push(PC & 0x000F);
    stack_push(PC & 0x00F0);
    PC = addr;
    Serial.println(addr, HEX);
}

void push_d9() {
    uint16_t val;
    read_d9(&ROM[PC], &val);
    stack_push(val);
    PC += 2;
}

inline void jmp() {
  
  
}

inline void jmpf() {
  // 16 bit jump instruction, moves the program counter
  // directly to the specified location
  PC = bytes_to_16(&ROM[PC + 1]);
  Serial.print("Jump to: ");
  Serial.println(PC);
}

inline void bne_i8r8() {
    uint8_t i8 = ROM[PC + 1];
    uint8_t r8 = ROM[PC + 2];

    if(*ACC != i8) {
        if(*ACC < i8) {
            set_carry();
        }
        PC += r8; 
    } else {
        PC += 3;
    }
}

inline void add_immediate() {
  // Adds the next value from ROM after PC to the ACC register (address 0x100)
  *ACC += *(ROM + PC + 1);  
  PC += 2; // Move the program counter after the value we added
}

inline void add_direct() {
  // Add the value from the address specified by the operand
  *ACC += RAM[ROM[PC + 1]];
  PC += 2;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(38400);
  Serial.println("Booting");
  delay(1000);

  *IE = 0x80; // Set to 10000000 (interuppts enabled)
  *SP = 0x7F; // Location of the stack
}

void loop() {
  // For debugging, print the next instruction in hex  
  Serial.print("Opcode: ");
  Serial.println(ROM[PC], HEX);
  
  // put your main code here, to run repeatedly:
  switch(ROM[PC]) {
    case 0x0: noop(); break;
    case 0x2: 
    case 0x3: ld(); break;
    case 0x12: st_d9(); break;
    case 0x18:
    case 0x19:
    case 0x1A:
    case 0x1B:
    case 0x1C:
    case 0x1D:
    case 0x1E:
    case 0x1F: call_a12(); break;
    case 0x21: jmpf(); break;
    case 0x22:
    case 0x23: mov_i8d9(); break;
    case 0x24:
    case 0x25:
    case 0x26:
    case 0x27: mov_rj(); break;
    case 0x41: bne_i8r8(); break;
    case 0x60: 
    case 0x61: push_d9(); break;
    case 0x62:
    case 0x63: inc_d9(); break;
    case 0xD8:
    case 0xD9:
    case 0xDA:
    case 0xDB:
    case 0xDC:
    case 0xDD:
    case 0xDE:
    case 0xDF: clr1(); break;
    case 0xE1: and_i8(); break;
    case 0xF1: xor_i8(); break;
    case 0xF2: xor_d9(); break;
    case 0xF8:
    case 0xF9:
    case 0xFA:
    case 0xFB:
    case 0xFC:
    case 0xFD:
    case 0xFE:
    case 0xFF: set1(); break;
    default: exit(0); // Unhandled opcode
  }  
}
